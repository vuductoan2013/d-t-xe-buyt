export interface IDesk {
  id?: number;
  price?: number;
}

export class Desk implements IDesk {

  constructor(public id: number,
              public price: number,
              ) {
    this.id = id;
    this.price = price;
  }
}
