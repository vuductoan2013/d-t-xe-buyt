import { IDesk } from './../model/desk.module';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test';
  array = [
    {id:1,price:100, isActive:false},
    {id:2,price:100, isActive:false},
    {id:3,price:100, isActive:false},
    {id:4,price:100, isActive:false},
    {id:5,price:100, isActive:false},
    {id:6,price:100, isActive:false},
    {id:7,price:100, isActive:false},
    {id:8,price:100, isActive:false},
    {id:9,price:100, isActive:false},
    {id:10,price:100, isActive:false},
    {id:11,price:100, isActive:false},
    {id:12,price:100, isActive:false},
  ];
  lists: any[] = [];
  numberDesk: number = 0;



  onAddDesk(i:number){
    const x = this.array.filter(x => x.id === (i+1));
    this.array[i].isActive = true;
    if(x && this.lists.filter(y => y.id === x[0].id).length ===0){
      this.lists.push(x[0]);
    }
    if(this.lists){
      this.numberDesk = this.lists.length;
    }
  }

  onRemoveDesk(j:number){
    const x = this.lists.filter(x => x.id === j);
    for(let i =0; i < this.lists.length; i++){
      if(this.lists[i].id === j){
        this.array.filter(y => y.id === this.lists[i].id)[0].isActive = false;
        this.lists.splice(i,1);
        this.numberDesk -= 1;
      }
    }

  }
}

